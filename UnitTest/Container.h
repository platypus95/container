#pragma once

#include <stdexcept>

template<typename T>
class Container
{
private:
	/////////////////////////
	///ATRIBUTE//////////////

	//Pointer of items
	T** items;
	//Number of memorie case reserve
	int nbCase;
	//The previous nb Case
	int previousNbCase;
	//Number of item
	int size;







	/////////////////////////
	///METHOD////////////////

	//Copy other container
	void Copy(const Container& obj) 
	{
		//Copy attribute
		nbCase = obj.nbCase;
		size = obj.size;

		//Copy all items
		items = new T*[nbCase];
		for (int i = 0; i < size; i++) {
			items[i] = new T;
			*items[i] = *obj.items[i];
		}
	}

	//Clear reservation of Dynamic allocation
	void ClearMemories()
	{
		for (int i = 0; i < size; i++) 
			delete[] items[i];
		delete[] items;
	}

	//Add memorie case reserved
	void MemoriesChange()
	{
		//Remember items in an buffer
		T** buffer = new T*[size];
		for (int i = 0; i < size; i++) {
			buffer[i] = new T;
			*buffer[i] = *items[i];
			delete[] items[i];
		}
		delete[] items;

		//Add the buffer's items in the new items
		items = new T*[nbCase];
		for (int i = 0; i < size; i++) {
			items[i] = new T;
			*items[i] = *buffer[i];
			delete[] buffer[i];
		}
		delete[] buffer;
	}

	//Check Size and Change memorie if need
	void CheckSize(bool remove)
	{
		//less memories
		if (remove) {
			if (size == previousNbCase - 1) {
				nbCase = previousNbCase;
				previousNbCase = previousNbCase == 10 ? 0 : previousNbCase / 2;
				MemoriesChange();
			}
		}
		//more memories
		else {
			if (size == nbCase) {
				previousNbCase = nbCase;
				nbCase *= 2;
				MemoriesChange();
			}
		}
	}








public:
	/////////////////////////
	///GETTER SETTER/////////

	//size
	const int& Size() const { return size; }
	//nbCase
	const int& NbCase() const { return nbCase; }
	//nbCase
	const int& PreviousNbCase() const { return previousNbCase; }








	//////////////////////////
	///CONSTRUCTOR///////////

	//Default 
	Container()
	: nbCase(10), size(0), previousNbCase(0)
	{ items = new T*[nbCase](); }
	
	//copy
	Container(const Container& obj) { Copy(obj); }

	//destructor
	~Container() { ClearMemories(); }









	/////////////////////////
	///METHOD////////////////

	//Add item
	void Add(const T& item) 
	{
		//Allocate Dynamic memory
		items[size] = new T;
		//copy the var in the memory
		*items[size++] = item;
		CheckSize(false);
	}

	//Delete last item
	void Pop() 
	{
		delete[] items[(size--)-1];
		CheckSize(true);
	}

	//Delete all items
	void Clear()
	{
		ClearMemories();
		nbCase = 10;
		previousNbCase = 0;
		size = 0;
		items = new T*[nbCase];
	}





	/////////////////////////
	///OPERATOR OVERLOAD/////

	//Copy
	Container& operator=(const Container& obj) 
	{
		ClearMemories();
		Copy(obj);
		return *this;
	}

	//Get an item
	T& operator[](int i) const
	{
		//Check if there is at least 1 item saved
		if (size == 0) {
			throw std::invalid_argument("no item in Container");
		}
		//If the index is not ok return the item at index 0
		if(i > size-1 || i < 0) {
			return *items[0];
		}

		return *items[i];
	}
};