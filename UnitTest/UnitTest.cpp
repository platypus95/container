#include "stdafx.h"
#include <gtest/gtest.h>

#include "Container.h"

//the number of items is supposed to be 0 at start
TEST(Container, SizeStart)
{
	Container<int> cont;
	EXPECT_EQ(0,cont.Size());
}

//throw an exception if you redirect [] when there is no item
TEST(Container, BrackectExceptionNoItems)
{
	Container<int> cont;
	EXPECT_ANY_THROW(cont[5]);
}

//Add item is working
TEST(Container, AddSuccess)
{
	Container<int> cont;
	cont.Add(5);
	EXPECT_EQ(5, cont[0]);
}

//Check after adding item if the size increment
TEST(Container, SizeIncrementAfterAdd)
{
	Container<int> cont;
	cont.Add(6);
	EXPECT_EQ(1, cont.Size());
}

//Bracket working
TEST(Container, getItem)
{
	Container<int> cont;
	cont.Add(5);
	EXPECT_EQ(5, cont[0]);
}

//Index 0 if index is not okay
TEST(Container, getItemIndexNotOkay)
{
	Container<int> cont;
	cont.Add(5);
	EXPECT_EQ(5, cont[-1]);
	EXPECT_EQ(5, cont[1]);
}

//Copy container to other container
TEST(Container, CopyOtherContainer1)
{
	Container<int> cont; cont.Add(5);
	Container<int> cont2 = cont;

	//Verify if its good Value
	EXPECT_EQ(5, cont2[0]);
}

//Copy container to other container
TEST(Container, CopyOtherContainer2)
{
	Container<int> cont; cont.Add(5); cont.Add(123); cont.Add(2345);
	Container<int> cont2 = cont;

	//Check the number of tiem
	EXPECT_EQ(123, cont2[1]);
}

//Copy container to other container
TEST(Container, CopyOtherContainer3)
{
	Container<int> cont; cont.Add(5); cont.Add(123); cont.Add(2345);
	Container<int> cont2 = cont;

	//Check the number of tiem
	EXPECT_EQ(3, cont2.Size());
}

//Copy container to other container
TEST(Container, CopyOtherContainer4)
{
	Container<int> cont; cont.Add(5); cont.Add(123); cont.Add(2345);
	Container<int> cont2;

	(cont2 = cont).Add(56);

	EXPECT_EQ(56, cont2[3]);
}

//Const container
TEST(Container, ConstantContainer)
{
	Container<int> cont; cont.Add(5); cont.Add(123); cont.Add(2345);
	const Container<int> cont2 = cont;

	EXPECT_EQ(2345, cont2[2]);
	EXPECT_EQ(3, cont2.Size());
}

//Const container
TEST(Container, AddItemChangeMemories)
{
	Container<int> cont; 
	cont.Add(5); 
	cont.Add(123); 
	cont.Add(2345);
	cont.Add(2345);
	cont.Add(2345);
	cont.Add(2345);
	cont.Add(2345);
	cont.Add(2345);
	cont.Add(2345);
	cont.Add(2345);

	EXPECT_EQ(123, cont[1]);

}

//Const container
TEST(Container, RemoveItemChangeMemories)
{
	Container<int> cont;
	cont.Add(5);
	cont.Add(123);
	cont.Add(2345);
	cont.Add(23445);
	cont.Add(23045);
	cont.Add(23425);
	cont.Add(2345);
	cont.Add(21345);
	cont.Add(234315);
	cont.Add(102345);
	cont.Add(2345123);

	cont.Pop();
	cont.Pop();

	EXPECT_EQ(0, cont.PreviousNbCase());
}

//Const container
TEST(Container, ClearItems)
{
	Container<int> cont;
	cont.Add(5);
	cont.Add(123);
	cont.Add(2345);

	cont.Clear();

	EXPECT_EQ(0, cont.Size());
	EXPECT_EQ(10, cont.NbCase());
	EXPECT_EQ(0, cont.PreviousNbCase());
}

//Const container
TEST(Container, UsePointerAsItem)
{
	int x = 5;
	Container<int*> cont;
	cont.Add(&x);

	*cont[0] = 7;


	EXPECT_EQ(7, x);
}

//Const container
TEST(Container, asda)
{
	Container<int> cont;

	cont.Add(0);
	cont.Add(1);
	cont.Add(2);
	cont.Add(3);
	cont.Add(4);
	cont.Add(5);
	cont.Add(6);
	cont.Add(7);
	cont.Add(8);
	cont.Add(9);

	//cont.Pop();

	EXPECT_EQ(10, cont.PreviousNbCase());
	EXPECT_EQ(9, cont[9]);
}

















int main(int argc, char** argv)
{
	testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

